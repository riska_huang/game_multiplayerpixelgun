using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerNameInputManager : MonoBehaviour
{
    public void SetPlayerName()
    {
        string playerName = GetComponent<TMP_InputField>().text;
        if (string.IsNullOrEmpty(playerName))
        {
            Debug.Log("player name is empty");
            return;
        }

        PhotonNetwork.NickName = playerName;
    }
}
